$(document).ready(function() {

    $('#contact_form').bootstrapValidator({
            live: 'enabled',

            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            message: ' '
                        },
                        notEmpty: {
                            message: 'Du mangler at udfylde dit fornavn'
                        }
                    }
                },
                last_name: {
                    validators: {
                        stringLength: {
                            min: 2,
                            message: ' '
                        },
                        notEmpty: {
                            message: 'Du mangler at udfylde dit efternavn'
                        }
                    }
                },

                phone: {
                    validators: {
                        stringLength: {
                            min: 8,
                            max:8
                        },
                        notEmpty: {
                            message: 'Du mangler at udfylde dit telefonnr.'
                        }
                    }
                },
                address: {
                    validators: {
                        stringLength: {
                            min: 3
                        },
                        notEmpty: {
                            message: 'Du skal skrive din adresse'
                        }
                    }
                },
                city: {
                    validators: {
                        stringLength: {
                            min: 4,
                            message: ' '
                        },
                        notEmpty: {
                            message: 'Du mangler at udfylde din by'
                        }
                    }
                },

                zip: {
                    validators: {
                        stringLength: {
                            max: 4
                        },

                        notEmpty: {
                            message: 'Du mangler at udfylde postnr.'
                        },

                    }
                },

            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
            //$('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();


            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            alert("SUCCESS");
            console.log(bv);

            //// Use Ajax to submit form data
            //$.post($form.attr('action'), $form.serialize(), function(result) {
            //    console.log(result);
            //}, 'json');
        });
});

