/**
 * Created by beuschau on 29/06/2016.

 */
$(document).ready(function () {



    $('#nav').affix({
        offset: {
            top: ($('section > .nav').outerHeight(true))+300,
            bottom: ($('.footer').outerHeight(true))+($('.footer-bottom').outerHeight(true))+100
        }
    });

    $(".nav li a[href^='#']").on('click', function (e) {

        e.preventDefault();

        var hash = this.hash;


        $('html, body')


            .animate({
                scrollTop: $(this.hash).offset().top - 130
            }, 800, function () {
                window.location.hash = hash;
            });

    });



    var transparent = true;

    if ( window.location.pathname != '/' ){
        $('header .navbar').addClass('colorBar').addClass('subPageHeader');
        $('.wrapper').addClass('colorMargin');

    } else {

        $(document).scroll(function() {

            if( $(this).scrollTop() > 90 ) {
                if(transparent) {
                    transparent = false;
                    $('header .navbar').addClass('colorBarAnimated');
                    console.log($(this).scrollTop());
                }
            } else {
                if( !transparent ) {
                    transparent = true;
                    $('header .navbar').removeClass('colorBarAnimated');
                }
            }
        });
    }





});






