var valgtadresse= null;
var map= null;
var korttype= null;
var host= "http://dawa.aws.dk";
var corssupported= "withCredentials" in (new XMLHttpRequest());

$( document ).ready(function() {

  var info= $("#adresseinfo"),
      ul= $('#autocomplete'),
      vejnavn = null,
      input= null;

  $('.ui-input-clear').on('click', function(e){
    input.val("");
    info.html( "" );
    info.listview( "refresh" );
  });



  function hentliste(q, caretpos) {
    info.html( "" );
    info.listview( "refresh" );
    ul.listview( "refresh" );
    var parametre=  {};
    parametre.side= 1;
    parametre.per_side= 4;
    parametre.q= q;
    parametre.type= "adresse";
    parametre.fuzzy= true;
    parametre.caretpos= caretpos;
    $.ajax({
        url: host+"/autocomplete",
        dataType: corssupported?"json":"jsonp",
        data: parametre
    })
    .then( function ( response ) {
      if (response.length === 1) {
        valgt(response[0]);
      }
      else {
        $.each( response, function ( i, val ) {
          ul.append("<li id='" + i + "'>" + val.forslagstekst + "</li>");
          $("#" + i).bind("vclick", chooseItem(val));
        });
      }
      ul.listview( "refresh" );
      $('#autocomplete li').removeClass('hidden ui-screen-hidden');
      //ul.trigger( "updatelayout");
    })
    .fail(function( jqXHR, textStatus, errorThrown ) {
      alert(jqXHR.responseText);
    });
  }

  function valgt(valg) {
    input.val(valg.tekst);
    if (valg.type === "adresse" ) {
      //visAdresseInfo(valg.data.href);
    }
    else {
      hentliste(valg.tekst, valg.caretpos);
    }
  }

  function chooseItem(valg) {
    return function (e) {
      ul.html( "" );
      ul.listview( "refresh" );
      valgt(valg);
      input.val(valg.tekst);
      input.focus();
      return false;
    };
  };

  $( "#autocomplete" ).on( "filterablebeforefilter", function ( e, data ) {

    ul = $( this );
    input = $( data.input );
    value = input.val();

    ul.html( "" );
    if ( value && value.length > 1 ) {
      hentliste(value,value.length);
    }

  });

});

