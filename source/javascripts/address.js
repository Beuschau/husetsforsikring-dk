"use strict";
/*global L:false */

var apiBase = 'http://dawa.aws.dk/';

function searchPostnr(input) {
  $.ajax({
    cache: true,
    url:apiBase +'postnumre',
    dataType: "json",
    error: function (xhr, status, errorThrown) {
      var text= xhr.status + " " + xhr.statusText + " " + status + " " + errorThrown;
      alert(text);
    } ,
    success: function (postnumre) {
      var items= [];
      $.each(postnumre, function (i, postnr) {
        items.push(postnr.nr + " " + postnr.navn);
      });
      $(input).autocomplete({
        source: items,
        autoFocus: true,
        minLength: 1
      });
    }
  });
}

function searchVejnavn(pnr,vej) {
  var ptext = $(pnr).val();
  var reg = /(\d{4})/g;
  var match = reg.exec(ptext);
  if (match === null) { return; }
  var parametre= {postnr: match[1]};
  $.ajax({
    url: apiBase + 'vejnavne',
    data: parametre,
    dataType: "json",
    error: function (xhr, status, errorThrown) {
      var text= xhr.status + " " + xhr.statusText + " " + status + " " + errorThrown;
      alert(text);
    } ,
    success: function (vejnavne) {
      var navne= [];
      $.each(vejnavne, function (i, vejnavn) {
        navne.push(vejnavn.navn);
      });
      $(vej).autocomplete({
        source: navne,
        autoFocus: true,
        minLength: 1
      });
    }
  });
}

function searchHusnr(pnr,vej,husnr) {
  var ptext = $(pnr).val();
  var reg = /(\d{4})/g;
  var match = reg.exec(ptext);
  if (match === null) { return; }
  var vtext = $(vej).val();
  if (vtext===null || vtext.length === 0) { return; }
  var parametre= {postnr: match[1], vejnavn: vtext};
  $.ajax({
    cache: true,
    url: apiBase + 'adresser',
    data: parametre,
    dataType: "json",
    error: function (xhr, status, errorThrown) {
      var text= xhr.status + " " + xhr.statusText + " " + status + " " + errorThrown;
      alert(text);
    } ,
    success: function (adresser) {
      var husnumre= [];
      $.each(adresser, function (i, adresse) {
        if (husnumre.indexOf(adresse.adgangsadresse.husnr) === -1) { husnumre.push(adresse.adgangsadresse.husnr); }
      });
      husnumre= husnumre.sort(function(a,b) {
                              var husnrreg= /(\d+)([A-Z]*)/gi;
                              var ma= husnrreg.exec(a);
                              husnrreg.lastIndex= 0;
                              var mb= husnrreg.exec(b);
                              if (ma === null || mb === null) { return 0; }
                              var ahusnr= ma[1];
                              var bhusnr= mb[1];
                              var abok= (ma[2] === '')?' ':ma[2];
                              var bbok= (mb[2] === '')?' ':mb[2];
                              return (ahusnr !== bhusnr)?(parseInt(ahusnr, 10) - parseInt(bhusnr, 10)):abok.localeCompare(bbok);
                            });
      $(husnr).autocomplete({
        source: husnumre,
        autoFocus: true,
        minLength: 1
      });
    }
  });
}


function searchEtage(pnr,vej,husnr,etage) {
  var ptext = $(pnr).val();
  var reg = /(\d{4})/g;
  var match = reg.exec(ptext);
  if (match === null) { return; }
  var vtext = $(vej).val();
  if (vtext===null || vtext.length === 0) { return; }
  var htext = $(husnr).val();
  if (htext===null || htext.length === 0) { return; }
  var parametre= {postnr: match[1], vejnavn: vtext, husnr: htext};
  $.ajax({
    cache: true,
    url: apiBase + 'adresser',
    data: parametre,
    dataType: "json",
    error: function (xhr, status, errorThrown) {
      var text= xhr.status + " " + xhr.statusText + " " + status + " " + errorThrown;
      alert(text);
    } ,
    success: function (adresser) {
      var etager= [];
      $.each(adresser, function (i, adresse) {
        if (etager.indexOf(adresse.etage) === -1) { etager.push(adresse.etage); }
      });
      $(etage).autocomplete({
        source: etager,
        autoFocus: true,
        minLength: 0
      });
      $(etage).autocomplete("search", "");
    }
  });
}


function searchDør(pnr,vej,husnr,etage,doer) {
  var ptext = $(pnr).val();
  var reg = /(\d{4})/g;
  var match = reg.exec(ptext);
  if (match === null) { return; }
  var vtext = $(vej).val();
  if (vtext===null || vtext.length === 0) return;
  var htext = $(husnr).val();
  if (htext===null || htext.length === 0) return;
  var etext = $(etage).val() || '';
  var parametre= {postnr: match[1], vejnavn: vtext, husnr: htext, etage: etext};
  $.ajax({
    cache: true,
    url:apiBase + 'adresser',
    data: parametre,
    dataType: "json",
    error: function (xhr, status, errorThrown) {
      var text= xhr.status + " " + xhr.statusText + " " + status + " " + errorThrown;
      alert(text);
    } ,
    success: function (adresser) {
      var dører= [];
      $.each(adresser, function (i, adresse) {
        if (dører.indexOf(adresse.dør) === -1) dører.push(adresse.dør);
      });
      $(doer).autocomplete({
        source: dører,
        autoFocus: true,
        minLength: 0
      });
      $(doer).autocomplete("search", "");
    }
  });
}



$(function () {
  function errorHandler(query) {
    return function(xhr, status, error) {
      $(query).text('(Fejl - ' + xhr.status + " " + xhr.statusText + " " + status + " " + error + ")");
    };
  }

  $('#autocomplete-adresse').dawaautocomplete({
    baseUrl: '',
    select: function(event, data) {
      $('#autocomplete-adresse-choice').text(data.tekst);
    },
    error: errorHandler('#autocomplete-adresse-choice')
  });
  $('#autocomplete-adgangsadresse').dawaautocomplete({
    adgangsadresserOnly: true,
    baseUrl: '',
    select: function(event, data) {
      $('#autocomplete-adgangsadresse-choice').text(data.tekst);
    },
    error: errorHandler('#autocomplete-adgangsadresse-choice')
  });
  $('#autocomplete-adresse-kbh').dawaautocomplete({
    baseUrl: '',
    params: {
      kommunekode: "101"
    },
    select: function(event, data) {
      $('#autocomplete-adresse-kbh-choice').text(data.tekst);
    },
    error: errorHandler('#autocomplete-adresse-kbh-choice')
  });


  var _postnummer = 'input[name=postnr]';
  var _address = 'input[name=adresse]';
  var _vejnr = 'input[name=vejnr]';
  searchPostnr( _postnummer);

  $(_address).focus(function () {
    searchVejnavn(_postnummer,_address);
  });
  $(_vejnr).focus(function () {
    searchHusnr(_postnummer,_address,_vejnr);
  });
  $('#etage').focus(function () {
    searchEtage(_postnummer,_address,_vejnr, '#etage');
  });
  $('#doer').focus(function () {
    searchDør(_postnummer,_address,_vejnr, '#etage', '#doer');
  });


});
