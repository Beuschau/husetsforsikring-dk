/**
 * Created by beuschau on 01/07/2016.
 */
function initialize() {

    var myLatLng = {lat: 55.6452338, lng: 12.0763484};

    var mapOptions = {
        center: myLatLng,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Danrisk'
    });

// To add the marker to the map, call setMap();
    marker.setMap(map);

   /* map.fitBounds(bounds);
    map.panToBounds(bounds);*/

}

google.maps.event.addDomListener(window, 'load', initialize);
