<?php
namespace Craft;

class ElektronikforsikringVariable
{
    public function orderTransaction($_id)
    {
       $trans = craft()->elektronikforsikring_orderTransaction->getTransactionDataWithOrderId($_id);
       return $trans[0];

    }

    public function getCardTypeId($_id)
    {
        $type = craft()->elektronikforsikring_orderTransaction->getPaymentTypeCardWithEpayId($_id);
        return $type;

    }

    public function globalCertificat()
    {
        $globalCertificat = craft()->globals->getSetByHandle('globalForsikringscertifikat');
        return $globalCertificat;

    }

    public function globalCreditCards()
    {
        $globaCreditCards = craft()->globals->getSetByHandle('globalCreditCard');
        return $globaCreditCards;

    }
    public function globalFooter()
    {
        $globaCreditCards = craft()->globals->getSetByHandle('globalFooter');
        return $globaCreditCards;

    }

    public function globalKurv()
    {
        $globaData= craft()->globals->getSetByHandle('globalKurv');
        return $globaData;

    }

    public function getPriceToday()
    {

        return craft()->elektronikforsikring_insurrencePrice->getPriceToday();

    }

    public function getPriceTodayTotal()
    {

        return craft()->elektronikforsikring_insurrencePrice->getPriceTodayTotal();

    }

    public function countOrders()
    {
       // return craft()->elektronikforsikring_sendOrders->countOrders();

    }

}