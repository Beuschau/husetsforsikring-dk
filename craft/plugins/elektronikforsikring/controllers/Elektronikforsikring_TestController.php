<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_TestController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array('actionCart','actionNewsletter','actionIndex','actionCertificate', 'actionEmailOrder', 'actionPdf','actionConditions'
    );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/elektronikforsikring
     */

    public function actionIndex()
    {

        $this->requirePostRequest();



        $var = array(

            'email' => craft()->request->getPost('email'),
            'isAjax' => craft()->request->isAjaxRequest(),

        );

        $sData = craft()->request->getPost('data');



       /* $cardInfo = craft()->elektronikforsikring_cardType->getPaymentType(3);

        $cart = craft()->commerce_cart->getCart();
        $cart->totalPrice = 300;


        foreach ($cart->adjustments as $item) {
            echo($item->name);

        }*/

        $this->returnJson($var);
    }


    public function actionCart()
    {
       echo "martin";

    }

    public function actionNewsletter()
    {
        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $this->renderTemplate('newsletter/signup');

    }


    public function actionPdf()
    {


        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $order = $this->_getOrders(1)[0];
        $pluginSettings = craft()->plugins->getPlugin('elektronikforsikring');
        $this->renderTemplate('commerce/_pdf/police', ['order' => $order, 'entry' => $pluginSettings]);


    }

    public function actionOrderTransaction()
    {
        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $order = $this->_getOrders(1)[0];
        $pluginSettings = craft()->plugins->getPlugin('elektronikforsikring');
        $this->renderTemplate('elektronikforsikring/test/orderTransaction', ['order' => $order, 'entry' => $pluginSettings]);


    }

    public function actionEmailOrder(array $variables = array())
    {


        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $order = $this->_getOrdersWithNumber($variables['orderid']);

        if (empty($order)) {
            craft()->request->redirect('/');
        }
        else
        {
            $this->renderTemplate('commerce/_emails/orderConfirmation', ['order' => $order]);
        }


    }

    public function actionGetTransactionFromOrder($_orderId = '1000015')
    {
        $orderId = $_orderId;
        $transactionData = craft()->elektronikforsikring_orderTransaction->getTransactionDataWithOrderId($orderId);
        return $transactionData[0];

    }

    public function actionOrders()
    {


        $_orders = $this->_getOrders();


        $_return = '';
        $separator = ';';

        $settings = craft()->plugins->getPlugin('elektronikforsikring')->getSettings();
        $forsikring = $settings->forsikring;
        $kladetype = $settings->kladetype;


        foreach ($_orders as $_order) {


            $transactionData = craft()->elektronikforsikring_orderTransaction->getTransactionDataWithOrderId($_order->id);
            $response = json_decode($transactionData[0]['response'], true);


            $txnfee = $transactionData[0]['txnfee'];
            $paymenttype = $transactionData[0]['paymenttype'];
            $cardInfo = craft()->elektronikforsikring_cardType->getPaymentType($paymenttype);
            $txnid = $response['txnid'];

            $_return .= '4' . $separator;
            $_return .= $_order->shippingAddress->firstName . " " . $_order->shippingAddress->lastName . $separator;
            $_return .= $_order->shippingAddress->address1 . $separator;
            $_return .= $_order->shippingAddress->zipCode . $separator;
            $_return .= $_order->shippingAddress->city . $separator;
            $_return .= $_order->shippingAddress->phone . $separator;
            $_return .= $_order->customer->email . $separator;
            $_return .= $_order->id . $separator;
            $_return .= $_order->dateOrdered . $separator;
            $_return .= $forsikring . $separator;
            $_return .= number_format($_order->totalPrice, 2, ',', '') . $separator;
            $_return .= $kladetype . $separator;
            $_return .= $txnid . $separator;
            $_return .= $cardInfo . $separator;
            $_return .= number_format($txnfee / 100, 2, ',', '') . $separator;
            $_return .= number_format($_order->lineItems[0]->shippingCost, 2, ',', '');
            $_return .= "\r\n";


            /**
             * Forhandlernr.
             * Kundenavn
             * Adresse
             * Postnr
             * By
             * Telefon
             * E-mail
             * Købsnota
             * Købsdato
             * Forsikring
             * Rapporteret præmie
             * Kladdetype
             * ePay Subscription ID
             * Card Type
             * Card Fee
             * Rest måned beløb
             **/
        }

        echo $_order->id;


    }

    public function actionTest()
    {
        $orders = craft()->elektronikforsikring_orderTransaction->getOrderWithTransactionData('1000015');
        var_dump($orders);





    }

    public function actionCertificate()
    {

        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $order = $this->_getOrders(1)[0];

        $this->renderTemplate('commerce/_pdf/orderCertificat', ['order' => $order]);



        /*
         *
         *
        {% set globalCertificat =  craft.elektronikforsikring.globalCertificat %}
        {% set subject = include(template_from_string(globalCertificat.pageTitle)|raw) %}
        {% set body = include(template_from_string(globalCertificat.body)|raw) %}
        {% set footerBody = include(template_from_string(globalCertificat.footer)|raw) %}
         */
    }

    private function guidv4()
    {
        if (function_exists('com_create_guid') === true)
            return trim(com_create_guid(), '{}');

        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    private function _getOrdersWithNumber($number)
    {


        $criteria = craft()->elements->getCriteria('Commerce_Order');
        $criteria->number = $number;

        return $criteria->first();

    }
    private function _getOrders($_limit = 3)
    {
        $orderStatusId = '2';
        // $limit = '10';

        $criteria = craft()->elements->getCriteria('Commerce_Order');
        $criteria->completed = true;
        $criteria->dateOrdered = "NOT NULL";
        $criteria->limit = $_limit;
        $criteria->order = 'dateOrdered desc';

        if ($orderStatusId) {
            $criteria->orderStatusId = $orderStatusId;
        }

        return $criteria->find();
    }

    public function actionConditions()
    {


        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $order = $this->_getOrders(1)[0];
        $pluginSettings = craft()->plugins->getPlugin('elektronikforsikring');
        $this->renderTemplate('includes/modal/betingelser', ['order' => $order, 'entry' => $pluginSettings]);


    }

}
