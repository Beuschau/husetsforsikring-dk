# elektronikforsikring plugin for Craft CMS

Variuos settings for order and sales activaties in commerce 

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install elektronikforsikring, follow these steps:

1. Download & unzip the file and place the `elektronikforsikring` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /elektronikforsikring`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `elektronikforsikring` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

elektronikforsikring works on Craft 2.4.x and Craft 2.5.x.

## elektronikforsikring Overview

-Insert text here-

## Configuring elektronikforsikring

-Insert text here-

## Using elektronikforsikring

-Insert text here-

## elektronikforsikring Roadmap

Some things to do, and ideas for potential features:

* Release it

## elektronikforsikring Changelog

### 1.0.0 -- 2016.06.27

* Initial release

Brought to you by [Martin Beuschau](18digital.dk)
