<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_InsurrencePriceService extends BaseApplicationComponent
{
    /**
     * This function can literally be anything you want, and you can have as many service functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     craft()->elektronikforsikring_sendOrders->exampleService()
     */
    const PRICE = 99;
    public $today = '';
    public $elektronikforsikring = '';


    public function init()
    {

        $today = getDate();

        $elektronikforsikring = new Elektronikforsikring_InsurrencePriceModel();
        $elektronikforsikring->setPricePerMonth(self::PRICE);
        $elektronikforsikring->setDaysInCurrentMonth($this->getDaysInMonth($today['mon'], $today['year']));

    }


    public function price_calendar()
    {

        $this->today = getDate();
        $nextTwelveMonths = 2;

        $tabledata = '';
        for ($i = 0; $i <= $nextTwelveMonths; $i++) {


            $month = mktime(0, 0, 0, date("m") + $i, date("d"), date("Y"));
            $key = date('m', $month);
            $year = date('Y', $month);
            $monthname = date('F', $month);
            $months[$key] = $monthname;


            $tabledata .= "<div class='col-md-4 col-sm-4 col-xs-4'>" . $this->build_calendar($key, $year) . "</div>";


        }

        return $tabledata;


    }

    private function getDaysInMonth($m, $y)
    {
        $_daysInMonth = cal_days_in_month(CAL_GREGORIAN, $m, $y);

        return $_daysInMonth;

    }

    public function getPriceTodayTotal()
    {
        $_today = getDate();
        $_thisMonth = $this->getPriceFromDate($_today['mday'], $_today['mon'], $_today['year']);
        $total = $_thisMonth + self::PRICE;

        return $total;

    }

    public function getPriceToday()
    {
        $_today = getDate();
        return $this->getPriceFromDate($_today['mday'], $_today['mon'], $_today['year']);

    }

    public function daysLeftInMonth()
    {
        $_today = getDate();
        $_daysInMonth = cal_days_in_month(CAL_GREGORIAN, $_today['mon'], $_today['year']);

        return $_daysInMonth-$_today['mday']+1;
    }

    public function getPriceFromDate($_date = null, $_month = null, $_year = null)
    {
        $_daysInMonth = cal_days_in_month(CAL_GREGORIAN, $_month, $_year);
        $pricePerDayNoFormat = (self::PRICE / $_daysInMonth) * ($_daysInMonth - ($_date - 1));

        return $pricePerDayNoFormat;


    }

    public function getMonthName($num)
    {
        $daysOfMonth = array('Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'Augutst', 'September', 'Oktober', 'November', 'December');
        return $daysOfMonth[$num-1];
    }

    private function build_calendar($month, $year)
    {

        //$daysOfMonth = array('Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'Augutst', 'September', 'Oktober', 'November', 'December');

        // What is the first day of the month in question?
        $firstDayOfMonth = mktime(0, 0, 0, $month, 1, $year);

        // How many days does this month contain?
        $numberDays = date('t', $firstDayOfMonth);

        // Retrieve some information about the first day of the
        // month in question.
        $dateComponents = getdate($firstDayOfMonth);

        // What is the name of the month in question?
        $monthName = $this->getMonthName($month - 1);

        // What is the index value (0-6) of the first day of the
        // month in question.
        $dayOfWeek = $dateComponents['wday'];

        $calendar = "<table class='table table-striped'>";
        $calendar .= "<caption style='text-align:center'>$monthName $year</caption>";
        $calendar .= "<tr>";

        // Initiate the day counter, starting with the 1st.

        $currentDay = 1;

        $month = str_pad($month, 2, "0", STR_PAD_LEFT);


        while ($currentDay <= $numberDays) {

            // Seventh column (Saturday) reached. Start a new row.

            if ($dayOfWeek == 7) {

                $dayOfWeek = 0;
                //$calendar .= "</tr><tr>";

            }

            $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);

            $date = "$currentDayRel-$month-$year";

            $d = strtotime($date);
            $class = '';
            if ($d == strtotime(date("Y-m-d"))) {
                $class = "style='border: 2px solid #ed2258'";
            }


            $pricePerDayNoFormat = $this->getPriceFromDate($currentDay, $month, $year);

            $pricePerDay = number_format($pricePerDayNoFormat, 2, ',', '');
            $totalPrice = number_format($pricePerDayNoFormat + self::PRICE, 2, ',', '');
            $toolTipContent = "Køber du vores Elektronikforsikring den " . date($date) . " betaler du <br>$pricePerDay kr. for resten af " . strtolower($monthName) . " og 99 kr. for efterfølgende måned. Samlet korttræk ved første betaling er derfor <b>$totalPrice kr.</b>";
            $calendar .= "<tr $class><td>$currentDay.</td></td><td class='day' rel='$date'><a href='#$date' data-trigger='hover' data-toggle='popover' data-placement='top' data-html='true' data-content='$toolTipContent' title='Ved køb den $date'>$pricePerDay kr.</a></td></tr>";


            // Increment counters

            $currentDay++;
            $dayOfWeek++;

        }


        // Complete the row of the last week in month, if necessary

        if ($dayOfWeek != 7) {

            $remainingDays = 7 - $dayOfWeek;
            $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>";

        }

        $calendar .= "</tr>";

        $calendar .= "</table>";

        return $calendar;

    }


}