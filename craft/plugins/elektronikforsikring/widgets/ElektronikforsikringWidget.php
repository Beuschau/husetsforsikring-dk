<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring Widget
 *
 * --snip--
 * Dashboard widgets allow you to display information in the Admin CP Dashboard.  Adding new types of widgets to
 * the dashboard couldn’t be easier in Craft
 *
 * https://craftcms.com/docs/plugins/widgets
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */
namespace Craft;
class ElektronikforsikringWidget extends BaseWidget
{
    /**
     * Returns the name of the widget name.
     *
     * @return mixed
     */
    public function getName()
    {
        return Craft::t('Elektronikforsikring');
    }

    /**
     * getBodyHtml() does just what it says: it returns your widget’s body HTML. We recommend that you store the
     * actual HTML in a template, and load it via craft()->templates->render().
     *
     * @return mixed
     */
    public function getBodyHtml()
    {
        // Include our Javascript & CSS
        craft()->templates->includeCssResource('elektronikforsikring/css/widgets/ElektronikforsikringWidget.css');
        craft()->templates->includeJsResource('elektronikforsikring/js/widgets/ElektronikforsikringWidget.js');
        /* -- Variables to pass down to our rendered template */
        $variables = array();
        $variables['settings'] = $this->getSettings();
        $orders = $this->_getOrders();

        return craft()->templates->render('elektronikforsikring/widgets/ElektronikforsikringWidget_Body', array(
            'orders' => $orders,
            'showStatuses' => $variables
        ));

    }
    private function _getOrders()
    {
        $orderStatusId = '2';
        $limit = '10';

        $criteria = craft()->elements->getCriteria('Commerce_Order');
        $criteria->completed = true;
        $criteria->dateOrdered = "NOT NULL";
        $criteria->limit = $limit;
        $criteria->order = 'dateOrdered desc';

        if($orderStatusId)
        {
            $criteria->orderStatusId = $orderStatusId;
        }

        return $criteria->find();
    }

    /**
     * Returns how many columns the widget will span in the Admin CP
     *
     * @return int
     */
    public function getColspan()
    {
        return 3;
    }

    /**
     * Defines the attributes that model your Widget's available settings.
     *
     * @return array
     */
    protected function defineSettings()
    {
        return array(
            'someSetting' => array(AttributeType::String, 'label' => 'Some Setting', 'default' => ''),
            'navisionEmail' => array(AttributeType::String, 'label' => 'Navision e-mail', 'default' => 'elektronikforsikring@elektronikforsikring.dk'),
        );
    }

    /**
     * Returns the HTML that displays your Widget's settings.
     *
     * @return mixed
     */
    public function getSettingsHtml()
    {

        /* -- Variables to pass down to our rendered template */

        $variables = array();
        $variables['settings'] = $this->getSettings();
        return craft()->templates->render('elektronikforsikring/widgets/ElektronikforsikringWidget_Settings', $variables);
    }

    /**
     * If you need to do any processing on your settings’ post data before they’re saved to the database, you can
     * do it with the prepSettings() method:
     *
     * @param mixed $settings The Widget's settings
     *
     * @return mixed
     */
    public function prepSettings($settings)
    {

        /* -- Modify $settings here... */

        return $settings;
    }
}