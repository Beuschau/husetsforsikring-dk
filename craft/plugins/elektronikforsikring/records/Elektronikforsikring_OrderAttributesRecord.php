<?php
namespace Craft;

class Elektronikforsikring_OrderAttributesRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'elektronikforsikring_orderattributes';
    }

    protected function defineAttributes()
    {
        return array(
            'txnfee' => AttributeType::String,
            'paymenttype' => AttributeType::Number,
            'currency' => AttributeType::Number,
        );
    }
}

