<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_InsurrencePriceModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */

    public function setPricePerMonth($_value)
    {

        return $this->setAttribute('pricePerMonth', $_value);
    }

    public function getPricePerMonth()
    {

        return $this->getAttribute('pricePerMonth');
    }


    public function getDaysInCurrentMonth()
    {

        return $this->getAttribute('daysInCurrentMonth');
    }

    public function setDaysInCurrentMonth($_value)
    {

        return $this->setAttribute('daysInCurrentMonth', $_value);
    }

    protected function defineAttributes()
    {

        return array(
            'pricePerMonth' => array(AttributeType::Number, 99 => ''),
            'daysInCurrentMonth' => array(AttributeType::Number, '' => ''),
        );


    }

}