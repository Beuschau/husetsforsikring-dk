<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'omitScriptNameInUrls' => true,
        'environmentVariables' => array(
            'baseUrl' => 'http://husetsforsikring.18digital.dev',
            'basePath' => getcwd(),
            'shopPrefix' => 'commerce' //<- add this line
        ),
        'mcsubApikey' => '62237acec28211459ead8a502e15a40b-us14',
        'mcsubListId' => 'c7c704aeb7',
        'mcsubDoubleOptIn' => false
    ),

    'husetsforsikring.18digital.dev' => array(
        'devMode' => true,
    ),

    'husetsforsikring.dk' => array(
        'cooldownDuration' => 0,
    ),

    'setPasswordPath' => array(
        'da' => 'forgotpassword',
    ),
);
